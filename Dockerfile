# renovate: datasource=docker depName=alpine
ARG ALPINE_VERSION=3.16
# renovate: datasource=docker depName=ruby versioning=ruby
ARG RUBY_VERSION=3.1.2

FROM ruby:$RUBY_VERSION-alpine$ALPINE_VERSION as development

RUN apk --no-cache upgrade && \
  apk add --no-cache \
  build-base \
  gcompat \
  less \
  postgresql-dev \
  shared-mime-info \
  nodejs \
  npm \
  tini \
  tzdata \
  vim \
  yarn \
  && rm -rf /var/cache/apk/*

WORKDIR /app

ENV EDITOR=vim
ENV RAILS_ENV=development
ENV RACK_ENV=development
ENV RAILS_LOG_TO_STDOUT=true
ENV RAILS_ROOT=/app
ENV LANG=C.UTF-8

# Install yarn packages
COPY package.json yarn.lock /app/
RUN yarn install --frozen-lockfile

# Install gems
COPY Gemfile* /app/
RUN gem update bundler && \
    bundle install --jobs "$(nproc)" --retry 2

COPY . /app

CMD ["bundle", "exec", "puma", "-b", "tcp://0.0.0.0:3000"]
ENTRYPOINT ["tini", "--", "/app/bin/entrypoint"]

FROM ruby:$RUBY_VERSION-alpine as production
ARG GID=10001
ARG UID=10000

RUN apk --no-cache upgrade && \
  apk add --no-cache \
  gcompat \
  nodejs \
  postgresql-dev \
  shared-mime-info \
  tini \
  tzdata \
  yarn \
  && rm -rf /var/cache/apk/*

RUN addgroup -S --gid $GID app && \
  adduser -S -G app -u $UID -s /bin/sh -h /app app
USER root

WORKDIR /app

ENV LANG=C.UTF-8
ENV RACK_ENV=production
ENV RAILS_ENV=production
ENV RAILS_LOG_TO_STDOUT=true
ENV RAILS_ROOT=/app
ENV RAILS_SERVE_STATIC_FILES=true
ENV SECRET_KEY_BASE=something

COPY --chown=$UID:$GID --from=development /app ./
COPY --chown=$UID:$GID --from=development /usr/local/bundle /usr/local/bundle

RUN bundle config set without 'development test' \
    && bundle clean --force \
    # Remove unneeded files (cached *.gem, *.o, *.c)
    && find /usr/local/bundle -name "*.gem" -delete \
    && find /usr/local/bundle -name "*.c" -delete \
    && find /usr/local/bundle -name "*.o" -delete

RUN apk add --no-cache yarn && \
    RAILS_ENV=production NODE_ENV=production bundle exec rails assets:precompile && \
    apk del yarn 

RUN rm -rf node_modules tmp/* log/* app/assets/builds/*

User app

EXPOSE 3000

CMD ["bundle", "exec", "puma", "-b", "tcp://0.0.0.0:3000"]
ENTRYPOINT ["tini", "--", "/app/bin/entrypoint"]
