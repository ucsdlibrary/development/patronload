require "rails_helper"

RSpec.describe "Patrons", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get patrons_path
      expect(response).to have_http_status(:success)
    end
  end
end
